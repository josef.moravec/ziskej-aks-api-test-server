'use strict';

var utils = require('../utils/writer.js');
var Callbacks = require('../service/CallbacksService');

module.exports.register_callback = function register_callback (req, res, next, body, sigla, callback_id) {
  Callbacks.register_callback(body, sigla, callback_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
