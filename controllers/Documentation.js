'use strict';

var utils = require('../utils/writer.js');
var Documentation = require('../service/DocumentationService');

module.exports.documentation = function documentation (req, res, next) {
  Documentation.documentation()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
