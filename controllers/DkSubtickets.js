'use strict';

var utils = require('../utils/writer.js');
var DkSubtickets = require('../service/DkSubticketsService');

module.exports.dk_subticket = function dk_subticket (req, res, next, sigla, subticket_id) {
  DkSubtickets.dk_subticket(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_cancel_get = function dk_subticket_cancel_get (req, res, next, sigla, subticket_id) {
  DkSubtickets.dk_subticket_cancel_get(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_cancel_post = function dk_subticket_cancel_post (req, res, next, body, sigla, subticket_id) {
  DkSubtickets.dk_subticket_cancel_post(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_complaint_get = function dk_subticket_complaint_get (req, res, next, sigla, subticket_id) {
  DkSubtickets.dk_subticket_complaint_get(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_complaint_post = function dk_subticket_complaint_post (req, res, next, body, sigla, subticket_id) {
  DkSubtickets.dk_subticket_complaint_post(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_dk_info = function dk_subticket_dk_info (req, res, next, body, sigla, subticket_id) {
  DkSubtickets.dk_subticket_dk_info(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_messages_get = function dk_subticket_messages_get (req, res, next, sigla, subticket_id) {
  DkSubtickets.dk_subticket_messages_get(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_messages_post = function dk_subticket_messages_post (req, res, next, body, sigla, subticket_id) {
  DkSubtickets.dk_subticket_messages_post(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_pm_unread = function dk_subticket_pm_unread (req, res, next, body, sigla, subticket_id) {
  DkSubtickets.dk_subticket_pm_unread(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subticket_transition = function dk_subticket_transition (req, res, next, body, sigla, subticket_id) {
  DkSubtickets.dk_subticket_transition(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dk_subtickets = function dk_subtickets (req, res, next, sigla, status, is_open, open_and_closed_in, expand) {
  DkSubtickets.dk_subtickets(sigla, status, is_open, open_and_closed_in, expand)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
