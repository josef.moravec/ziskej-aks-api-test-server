'use strict';

var utils = require('../utils/writer.js');
var ZkSubtickets = require('../service/ZkSubticketsService');

module.exports.zk_subticket = function zk_subticket (req, res, next, sigla, subticket_id) {
  ZkSubtickets.zk_subticket(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_cancel_get = function zk_subticket_cancel_get (req, res, next, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_cancel_get(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_cancel_post = function zk_subticket_cancel_post (req, res, next, body, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_cancel_post(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_complaint_get = function zk_subticket_complaint_get (req, res, next, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_complaint_get(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_complaint_post = function zk_subticket_complaint_post (req, res, next, body, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_complaint_post(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_messages_get = function zk_subticket_messages_get (req, res, next, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_messages_get(sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_messages_post = function zk_subticket_messages_post (req, res, next, body, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_messages_post(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_pm_unread = function zk_subticket_pm_unread (req, res, next, body, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_pm_unread(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subticket_transition = function zk_subticket_transition (req, res, next, body, sigla, subticket_id) {
  ZkSubtickets.zk_subticket_transition(body, sigla, subticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_subtickets = function zk_subtickets (req, res, next, sigla, status, is_open, open_and_closed_in, expand) {
  ZkSubtickets.zk_subtickets(sigla, status, is_open, open_and_closed_in, expand)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
