'use strict';

var utils = require('../utils/writer.js');
var Tickets = require('../service/TicketsService');

module.exports.zk_ticket = function zk_ticket (req, res, next, sigla, ticket_id) {
  Tickets.zk_ticket(sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_create = function zk_ticket_create (req, res, next, body, sigla) {
  Tickets.zk_ticket_create(body, sigla)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_dk_auto = function zk_ticket_dk_auto (req, res, next, body, sigla, ticket_id) {
  Tickets.zk_ticket_dk_auto(body, sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_dk_manual = function zk_ticket_dk_manual (req, res, next, body, sigla, ticket_id) {
  Tickets.zk_ticket_dk_manual(body, sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_dk_manual_dklist = function zk_ticket_dk_manual_dklist (req, res, next, sigla, ticket_id) {
  Tickets.zk_ticket_dk_manual_dklist(sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_dk_order_get = function zk_ticket_dk_order_get (req, res, next, sigla, ticket_id) {
  Tickets.zk_ticket_dk_order_get(sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_dk_order_put = function zk_ticket_dk_order_put (req, res, next, body, sigla, ticket_id) {
  Tickets.zk_ticket_dk_order_put(body, sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_reader = function zk_ticket_reader (req, res, next, body, sigla, ticket_id) {
  Tickets.zk_ticket_reader(body, sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_reader_info = function zk_ticket_reader_info (req, res, next, body, sigla, ticket_id) {
  Tickets.zk_ticket_reader_info(body, sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_ticket_transition = function zk_ticket_transition (req, res, next, body, sigla, ticket_id) {
  Tickets.zk_ticket_transition(body, sigla, ticket_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.zk_tickets = function zk_tickets (req, res, next, sigla, eppn, reader_id, reader_aks_id, status, is_open, open_and_closed_in, expand) {
  Tickets.zk_tickets(sigla, eppn, reader_id, reader_aks_id, status, is_open, open_and_closed_in, expand)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
