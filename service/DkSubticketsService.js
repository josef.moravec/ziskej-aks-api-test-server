'use strict';


/**
 * Subticket detail
 * Returns subticket detail for library in DK role.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns DKSubticket
 **/
exports.dk_subticket = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "hid" : 0,
  "is_open" : true,
  "is_snail_mail" : true,
  "doc_id" : "doc_id",
  "created_datetime" : "2000-01-23T04:56:07.000+00:00",
  "status_label" : "status_label",
  "sigla_zk" : "sigla_zk",
  "count_messages_unread" : 1,
  "subticket_url" : "http://example.com/aeiou",
  "bibliographic_reference" : "bibliographic_reference",
  "updated_datetime" : "2000-01-23T04:56:07.000+00:00",
  "count_messages" : 6,
  "doc_dk_id" : "doc_dk_id",
  "rfid" : "rfid",
  "subticket_id" : "subticket_id",
  "barcode" : "barcode",
  "subticket_type" : "mvs",
  "status" : "queued"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Cancel status
 * Vrátí stav z hlediska storna požadavku. Zda byla podána žádost o storno.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns DKSubticketCancelStatus
 **/
exports.dk_subticket_cancel_get = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "asked" : true
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change cancel status
 * FIXME
 *
 * body DKSubticketAskedCancelDecide 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.dk_subticket_cancel_post = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Complaint status
 * Vrátí stav z hlediska reklamace požadavku. Zda byl reklamován požadavek, zda byla podána námitka k SC.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns DKSubticketComplaintStatus
 **/
exports.dk_subticket_complaint_get = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "complaint_type" : "repeat",
  "refused" : true,
  "complained" : true,
  "objected" : true,
  "complaint_text" : "complaint_text"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change complaint status
 * FIXME
 *
 * body DKSubticketComplaintDecide 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.dk_subticket_complaint_post = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Set DK info
 * Set subticket information as DK.
 *
 * body DKInfo 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.dk_subticket_dk_info = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Subticket messages
 * Returns subticket messages for library in DK role.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns Messages
 **/
exports.dk_subticket_messages_get = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "items" : [ {
    "sender" : "library_zk",
    "unread" : true,
    "text" : "text",
    "created_datetime" : "2000-01-23T04:56:07.000+00:00"
  }, {
    "sender" : "library_zk",
    "unread" : true,
    "text" : "text",
    "created_datetime" : "2000-01-23T04:56:07.000+00:00"
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Send message
 * Send subticket message for library in DK role.
 *
 * body NewMessage 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.dk_subticket_messages_post = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Mark messages read
 * Set unread flag for messages for library in DK role.  Only value false is valid.
 *
 * body MessageUnread 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.dk_subticket_pm_unread = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Subticket workflow transition
 * Request subticket workflow transition for library in DK role.
 *
 * body DKSubticketWorkflowTransition 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.dk_subticket_transition = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Subtickets listing DK
 * Returns list of all tickets for library in DK role.
 *
 * sigla String Library SIGLA
 * status SubticketStatusEnum Only subtickets with this status. (optional)
 * is_open Boolean Only open / closed subtickets based on value true / false. (optional)
 * open_and_closed_in String Only open tickets or closed in last value days. (optional)
 * expand String For value detail return ticket details in response instead of ticket ids. (optional)
 * returns inline_response_200_2
 **/
exports.dk_subtickets = function(sigla,status,is_open,open_and_closed_in,expand) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

