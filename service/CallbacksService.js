'use strict';


/**
 * Register callback
 * Registruje callback AKS, který systém ZÍSKEJ zavolá, pokud nastane určitá událost v ZÍSKEJ. Prázdné url znamená smazání příslušného callbacku. Události v ZÍSKEJ (callback_id):   * `event_reader_ticket_changed` - došlo ke změně stavu objednávky čtenáře, dostupné placeholdery: eppn, reader_id, reader_aks_id, ticket_id, doc_dk_id, rfid, barcode   * `event_zk_subticket_changed` - doško ke změně požadavku knihovny v roli ŽK, dostupné placeholdery: eppn, reader_id, reader_aks_id, ticket_id, subticket_id, doc_dk_id, rfid, barcode   * `event_dk_subticket_changed` - doško ke změně požadavku knihovny v roli DK, dostupné placeholdery: subticket_id, doc_dk_id, rfid, barcodee Parametry v zaregistrovaných url mohou obsahovat placeholdery pro proměnné, např. {eppn}. Seznam placeholderů:   * `eppn`   * `reader_id`   * `reader_aks_id`   * `ticket_id`   * `subticket_id`   * `doc_dk_id`   * `rfid`   * `barcode` 
 *
 * body Callback 
 * sigla String Library SIGLA
 * callback_id String 
 * no response value expected for this operation
 **/
exports.register_callback = function(body,sigla,callback_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

