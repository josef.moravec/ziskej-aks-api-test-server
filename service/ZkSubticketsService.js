'use strict';


/**
 * Subticket detail
 * Vrátí detail požadavku v roli ŽK, aktivní MVS.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns ZKSubticket
 **/
exports.zk_subticket = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "hid" : 0,
  "closed_date" : "2000-01-23",
  "is_open" : true,
  "sent_date" : "2000-01-23",
  "sent_back_date" : "2000-01-23",
  "is_snail_mail" : true,
  "back_date" : "2000-01-23",
  "ticket_id" : "ticket_id",
  "doc_id" : "doc_id",
  "created_datetime" : "2000-01-23T04:56:07.000+00:00",
  "status_label" : "status_label",
  "count_messages_unread" : 1,
  "subticket_url" : "http://example.com/aeiou",
  "bibliographic_reference" : "bibliographic_reference",
  "updated_datetime" : "2000-01-23T04:56:07.000+00:00",
  "count_messages" : 6,
  "sigla_dk" : "sigla_dk",
  "accepted_date" : "2000-01-23",
  "doc_dk_id" : "doc_dk_id",
  "rfid" : "rfid",
  "subticket_id" : "subticket_id",
  "barcode" : "barcode",
  "subticket_type" : "mvs",
  "status" : "queued"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Cancel status
 * Vrátí stav z hlediska storna požadavku. Zda probíhá žádost o storno, zda je možné přímo stornovat, zda je možné zažádat o stornování.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns ZKSubticketCancelStatus
 **/
exports.zk_subticket_cancel_get = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "cancel_status" : "cannot"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change cancel status
 * FIXME
 *
 * body Subticket_id_cancel_body 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.zk_subticket_cancel_post = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Complaint status
 * Vrátí stav z hlediska reklamace požadavku. Zda probíhá reklamace, zda probíhá námitka k SC, zda je možné reklamovat, zda je možné podat námitku k SC.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns ZKSubticketComplaintStatus
 **/
exports.zk_subticket_complaint_get = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "complaint_status" : "cannot"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change complaint status
 * FIXME
 *
 * body Subticket_id_complaint_body 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.zk_subticket_complaint_post = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Subticket messages
 * Returns subticket messages for library in ZK role.
 *
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * returns Messages
 **/
exports.zk_subticket_messages_get = function(sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "items" : [ {
    "sender" : "library_zk",
    "unread" : true,
    "text" : "text",
    "created_datetime" : "2000-01-23T04:56:07.000+00:00"
  }, {
    "sender" : "library_zk",
    "unread" : true,
    "text" : "text",
    "created_datetime" : "2000-01-23T04:56:07.000+00:00"
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Send message
 * Send subticket message for library in ZK role.
 *
 * body NewMessage 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.zk_subticket_messages_post = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Mark messages read
 * Set unread flag for messages for library in ZK role.  Only value false is valid.
 *
 * body MessageUnread 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.zk_subticket_pm_unread = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Subticket workflow transition
 * Request subticket workflow transition for library in ZK role.
 *
 * body ZKSubticketWorkflowTransition 
 * sigla String Library SIGLA
 * subticket_id String Subticket ID
 * no response value expected for this operation
 **/
exports.zk_subticket_transition = function(body,sigla,subticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Subtickets listing ZK
 * Vrátí seznam všech požadavků dané knihovny v roli ŽK (aktivní MVS). Z parametrů status, is_open a open_and_closed_in lze použít maximálně jeden. Parametr expand nelze použít, pokud není použit parametr status, is_open nebo open_and_closed_in.
 *
 * sigla String Library SIGLA
 * status SubticketStatusEnum Filtr požadavků. Seznam hodnot status:   * `queued` - Přidělený   * `conditionally_accepted` - Podmínečně přijatý   * `accepted` - Přijatý, ve zpracování   * `sent` - Odeslaný   * `sent_back` - Odeslaný zpět   * `closed` - Uzavřený  (optional)
 * is_open Boolean Filtr požadavků. Pro true vrátí seznam aktivních požadavků, pro false seznam požadavků, které aktivní nejsou. Požadavek je aktivní, pokud není uzavřený, stornovaný nebo reklamovaný. (optional)
 * open_and_closed_in String Filtr požadavků. Vrátí seznam aktivních požadavků a těch, které byly uzavřeny v posledních `value` dnech. (optional)
 * expand ExpandEnum For value detail return ticket details in response instead of ticket ids. (optional)
 * returns inline_response_200_1
 **/
exports.zk_subtickets = function(sigla,status,is_open,open_and_closed_in,expand) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

