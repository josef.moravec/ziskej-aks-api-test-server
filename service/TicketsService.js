'use strict';

var generator = require('../utils/generator.js');

/**
 * Ticket detail
 * Vrátí detail objednávky. Seznam hodnot status:
 * * `created` - Nová
 * * `assigned` - Ve zpracování (byla schválena a byl vytvořen a přidělen požadavek, který je stále aktivní)
 * * `pending` - Ve zpracování (byla schválena a byl vytvořen a přidělen požadavek, který ale už není aktivní a ani žádný jiný není aktivní)
 * * `prepared` - Připraveno pro čtenáře
 * * `lent` - Půjčeno čtenáři
 * * `returned` - Vráceno čtenářem
 * * `closed` - Uzavřená (čtenář vrátil nebo naopak nevyzvedl) Objednávka je aktivní, pokud není uzavřená, stornovaná nebo reklamovaná.
 *
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * returns Ticket
 **/
exports.zk_ticket = function(sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    const fields = {
      "ticket_type" : "tickettype",
      "hid" : "integer",
      "eppn" : "string",
      "reader_lid" : "integer",
      "created_datetime" : "datetime",
      "reader_aks_id" : "integer",
      "reader_from_date" : "date",
      "ticket_doc_data_source" : "datasource",
      "updated_datetime" : "datetime",
      "subticket_id" : "integer",
      "barcode" : "string",
      "reader_id" : "integer",
      "is_open" : "boolean",
      "ticket_id" : "integer",
      "ticket_url" : "url",
      "subticket_ids" : [ "integer" ],
      "doc_id" : "integer",
      "reader_note" : "string",
      "reader_place" : "string",
      "status_label" : "string",
      "reader_to_date" : "date",
      "bibliographic_reference" : "string",
      "doc_dk_id" : "string",
      "rfid" : "string",
      "status" : "status"
    };
    examples['application/json'] = generator.generate(fields)
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Create new ticket
 * Vytvoří novou asistovanou objednávku – automatickou (údaje z CPK) nebo manuální (volný formulář).
 *
 * body Sigla_tickets_body 
 * sigla String Library SIGLA
 * returns TicketId
 **/
exports.zk_ticket_create = function(body,sigla) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Choose DK auto
 * Zvolit automatickou volbu dožádaných knihoven, iniciovat načtení dostupnosti z CPK a po načtení nechat systém navrhnout pořadí DK.  Následně je možné získat pořadí DK navržené systémem, to převzít nebo změnit a určit pro jednotlivé položky způsob dodání. Je k dispozici pouze pro asistované objednávky automatické (zdrojem je CPK, je známo doc_id a seznam alternativních doc_id na základě informací z CPK. Zpracování požadavku trvá krátce, protože pouze se nečeká na načtení dostupnosti, to ale obvykle trvá velmi dlouho v závislosti na rychlosti dotazovaných systémů.  Pokud je použito API pro získání navrženého pořadí před ukončením načítání dostupnosti, je vrácena chyba.
 *
 * body TicketDkAuto 
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * no response value expected for this operation
 **/
exports.zk_ticket_dk_auto = function(body,sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Choose DK manual
 * Zvolit manuální volbu dožádané knihovny, dožádanou knihovnu a způsob dodání. Hodnota lb_manual_library musí být value jedné z položek z volání \"Seznam dožádatelných knihoven pro tuto objednávku\".
 *
 * body TicketDkManual 
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * no response value expected for this operation
 **/
exports.zk_ticket_dk_manual = function(body,sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * DK list usable for manual selection of DK
 * Vrátí seznam knihoven, které je možné dožádat manuálně pro tuto objednávku.
 *
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * returns TicketManualDkList
 **/
exports.zk_ticket_dk_manual_dklist = function(sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "dk_list" : [ {
    "title" : "title",
    "value" : "value"
  }, {
    "title" : "title",
    "value" : "value"
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * DK order
 * Získat pořadí DK navržené systémem pro automatickou volbu DK. Je možné volat až po volání \"Zvolit automaticky DK a nechat systém navrhnout pořadí DK\". Vrací chybu 400 pokud zatím nedoběhlo načtení dostupnosti.
 *
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * returns TicketSuggestedDkList
 **/
exports.zk_ticket_dk_order_get = function(sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "dk_list" : [ {
    "note_html" : "note_html",
    "title" : "title",
    "doc_id" : "doc_id",
    "is_eligible" : true
  }, {
    "note_html" : "note_html",
    "title" : "title",
    "doc_id" : "doc_id",
    "is_eligible" : true
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Choose DK order
 * Určí pořadí prvních třech automaticky připřazených DK a způsob dodání pro jednotlivé DK.  Ve skutečnosti se volí přímo dokumenty v CPK pomocí doc_id.  Pouze hodnoty z volání \"Získat pořadí DK navržené systémem\" z položek s is_eligible true jsou přípustné. Pro každou položku je potřeba navíc určit způsob dodání. Je možné volat až po volání \"Zvolit automaticky DK a nechat systém navrhnout pořadí DK\".
 *
 * body TicketDkOrder 
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * no response value expected for this operation
 **/
exports.zk_ticket_dk_order_put = function(body,sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Set reader details
 * Nastavit informace o čtenáři a zejména jeho ověření. Je podmínkou pro vytvoření požadavku na dožádanou knihovnu.
 *
 * body TicketReader 
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * no response value expected for this operation
 **/
exports.zk_ticket_reader = function(body,sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Set details for reader
 * Pro objednávku nastaví informace pro čtenáře k vyzvednutí.  Vždy nastavuje všechny atributy.
 *
 * body ReaderInfo 
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * no response value expected for this operation
 **/
exports.zk_ticket_reader_info = function(body,sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Ticket workflow transition
 * Vyžádat změnu stavu objednávky.
 *
 * body TicketWorkflowTransition 
 * sigla String Library SIGLA
 * ticket_id String Ticket ID
 * no response value expected for this operation
 **/
exports.zk_ticket_transition = function(body,sigla,ticket_id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Tickets listing
 * Vrátí seznam všech objednávek knihovny, resp. podle příslušného filtru. Z parametrů eppn, reader_id a reader_aks_id lze použít maximálně jeden. Z parametrů status, is_open a open_and_closed_in lze použít maximálně jeden. Parametr expand nelze použít, pokud není použit parametr status, is_open nebo open_and_closed_in.
 *
 * sigla String Library SIGLA
 * eppn String Filtr objednávek. Eppn čtenáře v Shibboleth, prázdný string nebo null pro objednávky nevytvořené čtenářem v CPK UI. (optional)
 * reader_id String Filtr objednávek. ID čtenáře v Získej. (optional)
 * reader_aks_id String Filtr objednávek. ID čtenáře v AKS. (optional)
 * status TicketStatusEnum Filtr objednávek. Seznam hodnot status:   * `created` - Nová   * `assigned` - Ve zpracování (byla schválena a byl vytvořen a přidělen požadavek, který je stále aktivní)   * `pending` - Ve zpracování (byla schválena a byl vytvořen a přidělen požadavek, který ale už není aktivní a ani žádný jiný není aktivní)   * `prepared` - Připraveno pro čtenáře   * `lent` - Půjčeno čtenáři   * `returned` - Vráceno čtenářem   * `closed` - Uzavřená (čtenář vrátil nebo naopak nevyzvedl)  (optional)
 * is_open Boolean Filtr objednávek. Pro true vrátí seznam aktivních objednávek, pro false seznam objednávek, které aktivní nejsou. Objednávka je aktivní, pokud není uzavřená, stornovaná nebo reklamovaná. (optional)
 * open_and_closed_in Integer Filtr objednávek. Vrátí seznam aktivních objednávek a těch, které byly uzavřeny v posledních `value` dnech. (optional)
 * expand ExpandEnum For value detail return ticket details in response instead of ticket ids. (optional)
 * returns inline_response_200
 **/
exports.zk_tickets = function(sigla,eppn,reader_id,reader_aks_id,status,is_open,open_and_closed_in,expand) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json']['tickets'] = [{
      "ticket_type" : "mvs",
      "hid" : 0,
      "eppn" : "eppn",
      "reader_lid" : "reader_lid",
      "created_datetime" : "2000-01-23T04:56:07.000+00:00",
      "reader_aks_id" : "reader_aks_id",
      "reader_from_date" : "2000-01-23",
      "ticket_doc_data_source" : "auto",
      "updated_datetime" : "2000-01-23T04:56:07.000+00:00",
      "subticket_id" : "subticket_id",
      "barcode" : "barcode",
      "reader_id" : "reader_id",
      "is_open" : true,
      "ticket_id" : "ticket_id",
      "ticket_url" : "http://example.com/aeiou",
      "subticket_ids" : [ "subticket_ids", "subticket_ids" ],
      "doc_id" : "doc_id",
      "reader_note" : "reader_note",
      "reader_place" : "reader_place",
      "status_label" : "status_label",
      "reader_to_date" : "2000-01-23",
      "bibliographic_reference" : "bibliographic_reference",
      "doc_dk_id" : "doc_dk_id",
      "rfid" : "rfid",
      "status" : "created"
    }];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

