var capitalize = require('capitalize')

/**
 * Generate random data
 *
 * @param fields Fields definition. Example: ["id": "integer", name: "string"].
 * Available types are: integer, string, date, datetime, tickettype, boolean, datasource
 */
exports.generate = function(fields) {
    let data = {}
    for (const field in fields) {
        if (Array.isArray(fields[field])) {
            data[field] = [generateValueByType(fields[field][0])]
        } else {
            data[field] = generateValueByType(fields[field])
        }
    }
    return data
}

const generator = {
    generateInteger: function() {
        return Math.floor(Math.random() * 1000)
    },
    generateString: function() {
        return (Math.random() + 1).toString(36).substring(7)
    },
    generateDate: function() {
        return this.generateDatetime().substring(0, 10)
    },
    generateDatetime: function() {
        const start = new Date(2021, 11, 1)
        const end = new Date(2025, 12,31)
        const date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()))
        return date.toISOString()
    },
    generateTickettype: function() {
        return 'mvs'
    },
    generateDatasource: function() {
        return this.generateBoolean() ? 'auto': 'manual'
    },
    generateBoolean: function() {
        return Math.random() < 0.5
    },
    generateUrl: function() {
        return 'https://example.com/' + this.generateString()
    },
    generateStatus: function() {
        const statuses = ['created', 'assigned', 'pending', 'prepared', 'lent', 'returned', 'closed']
        return statuses[Math.floor(Math.random() * statuses.length)]
    }
}

function generateValueByType(type) {
    const method = 'generate' + capitalize(type)
    return generator[method]()
}
